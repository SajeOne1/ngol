//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// File: ngol.c
// Author: Shane "SajeOne" Brown
// Date: 2018-10-30
// Description: Randomized conway's game of life in ncurses
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#include "ngol.h"

int main(int argc, char* argv[]){
	setlocale(LC_ALL, "");
	int** front_board;
	int** back_board;
	MEVENT event;

	srand(time(NULL));

	//const char* SYMBOL = "\xE1\xB3\x83";
	//const char* SYMBOL = "\xE2\x8F\xBA";
	//const char* SYMBOL = "\xe2\x97\xa6";
	const char* SYMBOL = "\xE2\x97\x86";
	//const char* SYMBOL = "\xE2\x96\xA3";


	initscr();
	curs_set(0);
	timeout(1);
	cbreak();
	keypad(stdscr, TRUE);

	mousemask(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, NULL);

	int cols;
	int rows;
	getmaxyx(stdscr, rows, cols);
	back_board = board_create(rows, cols);
	front_board = board_create(rows, cols);
		
	
	int quit = 1;
	while(quit){
		board_tick(&front_board, &back_board, rows, cols);
		board_swap(&front_board, &back_board);

		clear();
		for(int i = 0; i < rows; i++){
			for(int k = 0; k < cols; k++){
				if(front_board[i][k]){
					mvaddstr(i, k, SYMBOL);
				}
			}
		}

		int key = getch();

		mvaddstr(rows/2, cols/2, "clicked");
		switch(key){
			case ' ':
				zero_board(front_board, rows, cols);
				break;
			case 'q':
				quit = 0;
			case KEY_MOUSE:
				if(getmouse(&event) == OK){
					generate_glider(front_board, event.x, event.y, cols, rows);
				}
				break;
			
		}

		if(1 && board_count(front_board, rows, cols) < 100){
			int r_row,r_col;
			int choice = rand() % 3;

			random_coord(&r_row, &r_col, rows, cols);
			switch(choice){
				case 0:
					generate_glider(front_board, r_row, r_col, cols, rows);
					break;
				case 1:
					generate_penta(front_board, r_row, r_col, cols, rows);
					break;
				case 2:
					generate_lwss(front_board, r_row, r_col, cols, rows);
			}

		}
		
		usleep(130000);
		refresh();
	}

	for(int i = 0; i < rows; i++){
		free(front_board[i]);
		free(back_board[i]);
	}
	free(front_board);
	free(back_board);

	endwin();
	return 0;
}

void board_swap(int*** sw1, int*** sw2){
	int** temp = *sw1;

	*sw1 = *sw2;
	*sw2 = temp;
}

int** board_create(int rows, int cols){
	int** copy;

	copy = calloc(rows, sizeof(int*));
	for(int i = 0; i < rows; i++){
		copy[i] = calloc(cols, sizeof(int));
	}

	return copy;
}

void zero_board(int** board, int rows, int cols){
	for(int i = 0; i < rows; i++){
		for(int k = 0; k < cols; k++){
			board[i][k] = 0;
		}
	}
}

void random_coord(int* row, int* col, int height, int width){
	*row = rand() % height;
	*col = rand() % width;
}

int board_count(int** board, int rows, int cols){
	int count = 0;
	for(int i = 0; i < rows; i++)
		for(int k = 0; k < cols; k++)
			if(board[i][k])
				count++;	

	return count;
}

void generate_gun(int** board, int x, int y, int width, int height){
	if(x+32 < (width-1) && y+13 < (height-1)){
		board[y][x] = 1;
		board[y+1][x] = 1;
		board[y+1][x+1] = 1;

		board[y+3][x+4] = 1;
		board[y+4][x+4] = 1;
		board[y+4][x+5] = 1;

		board[y][x+7] = 1;
		board[y+1][x+7] = 1;
		board[y+1][x+8] = 1;

		board[y+11][x+20] = 1;
		board[y+10][x+21] = 1;
		board[y+11][x+21] = 1;
		board[y+12][x+21] = 1;
		board[y+9][x+22] = 1;
		board[y+12][x+22] = 1;
		board[y+13][x+22] = 1;

		board[y+8][x+27] = 1;
		board[y+9][x+27] = 1;
		board[y+9][x+28] = 1;

		board[y+11][x+31] = 1;
		board[y+12][x+31] = 1;
		board[y+12][x+32] = 1;
	}
}

void generate_lwss(int** board, int x, int y, int width, int height){
	if(x+6 < (width-1) && y+4 < (height-1)){
		board[y+1][x] = 1;
		board[y+3][x] = 1;
		board[y+4][x+1] = 1;
		board[y][x+2] = 1;

		board[y+4][x+2] = 1;
		board[y][x+3] = 1;
		board[y+4][x+3] = 1;

		board[y+4][x+4] = 1;
		board[y+1][x+5] = 1;
		board[y+4][x+5] = 1;

		board[y+2][x+6] = 1;
		board[y+3][x+6] = 1;
		board[y+4][x+6] = 1;
	}
}

void generate_penta(int** board, int x, int y, int width, int height){
	if((height-1) - y > 10 && (width-1) - x > 3){
		board[y+1][x+1] = 1;
		board[y+1][x+2] = 1;
		board[y+1][x+4] = 1;
		board[y+1][x+5] = 1;
		board[y+1][x+6] = 1;
		board[y+1][x+7] = 1;
		board[y+1][x+9] = 1;
		board[y+1][x+10] = 1;


		board[y+2][x+3] = 1;
		board[y][x+3] = 1;
		board[y+2][x+8] = 1;
		board[y][x+8] = 1;
	}
}

void generate_glider(int** board, int x, int y, int width, int height){
	if((height-1) - y > 3 && (width-1) - x > 3){
		board[y][x+1] = 1;	
		board[y+1][x+2] = 1;
		board[y+2][x] = 1;
		board[y+2][x+1] = 1;
		board[y+2][x+2] = 1;
	}
}

void board_tick(int*** front, int*** back, int height, int width){
	int** board = (*front);
	int** new_board = (*back);

	zero_board(new_board, height, width);

	int n = 0;
	for(int row = 0; row < height; row++){
		for(int col = 0; col < width; col++){
			n = 0;
			//UP

			if(row > 0 && board[row-1][col]) n++;
			//DOWN
			if(row < height-1 && board[row+1][col]) n++;
			//LEFT
			if(col > 0 && board[row][col-1]) n++;
			//RIGHT
			if(col < width-1 && board[row][col+1]) n++;
			//UPLEFT
			if(col > 0 && row > 0 && board[row-1][col-1]) n++;
			//UPRIGHT
			if(row > 0 && col < width-1 && board[row-1][col+1]) n++;
			//DOWNLEFT
			if(row < height-1 && col > 0 && board[row+1][col-1]) n++;
			//DOWNRIGHT
			if(row < height-1 && col < width-1 && board[row+1][col+1]) n++;


			if(board[row][col]){ // ALIVE
				if(n > 3){
					new_board[row][col] = 0;
				}else if(n < 2){
					new_board[row][col] = 0;
				}else{ // U LIVE ON
					new_board[row][col] = 1;
				}
			}else{ // DEAD
				if(n == 3)
					new_board[row][col] = 1;
			}
		}
	}
}
