//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// File: ngol.h
// Author: Shane "SajeOne" Brown
// Date: 2018-10-30
// Description: Randomized conway's game of life in ncurses
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#ifndef NGOL_H
#define NGOL_H

#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <locale.h>
#include <unistd.h>
#include <time.h>

#include <ncurses.h>


void board_tick(int*** front, int*** back, int width, int height);
void board_swap(int*** sw1, int*** sw2);
int** board_create(int rows, int cols);
void zero_board(int** board, int rows, int cols);
void random_coord(int* row, int* col, int height, int width);
int board_count(int** board, int rows, int cols);
void generate_penta(int** board, int x, int y, int width, int height);
void generate_glider(int** board, int x, int y, int width, int height);
void generate_lwss(int** board, int x, int y, int width, int height);
void generate_gun(int** board, int x, int y, int width, int height);

#endif
